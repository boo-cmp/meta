#pragma once

#include "orm_base.h"

namespace orm
{

template <typename ValueType>
class value_field : public base_field, public meta::member_field<ValueType>
{
public:
  value_field(ValueType& _member, const ValueType& _def_value)
    : meta::member_field<ValueType>(_member, _def_value)
  {
    save_strategy = SaveStrategy::BEFORE_MASTER;
  }

  void save() override {}
  void load() override {}

  void set_null() override
  {
    null_setter<ValueType> set(member, default_value);
  }

  void capture(query::rows::getstream & r) override
  {
    value_capturer<ValueType> capturer(r, member);
  }

  void bind(statement & q) override
  {
    value_binder<ValueType> binder(q, ":" + meta().sql_name, member);
  }
};

}  // namespace orm
