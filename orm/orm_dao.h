#pragma once

#include "orm_value.h"

namespace orm
{

template <typename ModelType>
class dao : private ModelType
{
private:
  using T = ModelType;

  static database * db;

  void create_storage()
  {
    std::string q;

    if (T::is_temp())
    {
      q = "CREATE TEMP TABLE IF NOT EXISTS ";
      q.append(T::table_name()).append("(");
    }
    else
    {
      q = "CREATE TABLE IF NOT EXISTS ";
      q.append(T::table_name()).append("(");
    }
    for (auto field : T::get_fields_metadata<sql_desc>())
    {
      q.append(field->sql_name).append(" ").append(field->sql_type);

      if (field->sql_constraints == orm::PRIMARY_KEY)
        q.append(" PRIMARY KEY");

      q.append(",");
    }
    q.pop_back();
    q.append(")");

    if (SQLITE_OK != db->execute(q.c_str()))
    {
      throw database_error(("can't create table " + T::table_name() + " with message: " + db->error_msg()).c_str());
    }
  }

  template <typename storageT, typename O>
  static std::vector<std::shared_ptr<O> > objects_from_storage(storageT & storage)
  {
    std::vector<std::shared_ptr<O> > r;

    for (auto row : storage)
    {
      query::rows::getstream getter = row.getter();
      std::shared_ptr<O> obj(new O());
      bool isNull = true;
      for (auto field : obj->get_fields<base_field>())
      {
        if (getter.is_null())
        {
          field->set_null();
          int nl; getter >> nl;
        }
        else
        {
          field->capture(getter);
          isNull = false;
        }
      }
      if (!isNull)
      {
        r.push_back(obj);
      }
    }

    return r;
  }

public:

  dao(database & _db, bool _create_storage = false)
  {
    db = &_db;

    if (_create_storage)
    {
      create_storage();
    }
  }

  static std::unique_ptr<orm::transaction> transaction()
  {
    return std::make_unique<sqlite::orm::transaction>(*db);
  }

  template <typename V>
  static std::vector<std::shared_ptr<V>> query(const std::string & body)
  {
    sqlite3pp::query qry(*db, body.c_str());
    return objects_from_storage<sqlite3pp::query, V>(qry);
  }

  static std::vector<std::shared_ptr<T>> query(const std::string & where)
  {
    std::string q = "SELECT ";

    for (auto field : model<T>::get_fields_metadata<sql_desc>())
    {
      q.append(field->sql_select_stmt()).append(",");
    }

    q.pop_back();
    q.append(" FROM ").append(T::table_name());

    q.append(" ");
    q.append(where);

    sqlite3pp::query qry(*db, q.c_str());

    auto result = objects_from_storage<sqlite3pp::query, T>(qry);

    for (auto r : result)
    {
      for (auto f : r->fields)
      {
        f->load();
      }
    }

    return result;
  }

  template <typename V>
  static std::vector<std::shared_ptr<T>> query(const std::string & fn, const V & v)
  {
    std::string q = "SELECT ";

    for (auto field : model<T>::get_fields_metadata<sql_desc>())
    {
      q.append(field->sql_select_stmt).append(",");
    }

    q.pop_back();
    q.append(" FROM ").append(T::table_name());

    if (!fn.empty())
    {
      q.append(" WHERE ").append(fn).append(" = :var");
    }

    sqlite3pp::query qry(*db, q.c_str());

    if (!fn.empty())
    {
      auto cpy = v;
      value_binder<V> binder(qry, ":var", cpy);
    }

    auto result = objects_from_storage<sqlite3pp::query, T>(qry);

    for (auto r : result)
    {
      for (auto f : r->get_fields<base_field>())
      {
        f->load();
      }
    }

    return result;
  }

  template <typename V>
  static std::shared_ptr<T> query_one(const std::string & fn, const V & v)
  {
    auto r = query(fn, v);
    if (r.empty())
    {
      return std::shared_ptr<T>(nullptr);
    }
    return r.at(0);
  }

  static std::vector<std::shared_ptr<T>> query_all()
  {
    return query("", 0);
  }

  static void save(const std::shared_ptr<T> & obj)
  {
    if (obj.get())
    {
      save(*obj.get());
    }
  }

  static void save(const T & obj)
  {
    std::string q = "INSERT OR REPLACE INTO ";
    q.append(T::table_name());
    q.append(" (");

    for (auto field : model<T>::get_fields_metadata<sql_desc>())
    {
      q.append(field->sql_name).append(",");
    }

    q.pop_back();
    q.append(") VALUES (");

    for (auto field : model<T>::get_fields_metadata<sql_desc>())
    {
      q.append(":" + field->sql_name).append(",");
    }

    q.pop_back();
    q.append(")");

    sqlite3pp::command qry(*db, q.c_str());

    for (auto field : obj.get_fields<base_field>())
    {
      if (field->save_strategy == base_field::SaveStrategy::BEFORE_MASTER)
      {
        field->save();
      }
      field->bind(qry);
    }

    if (qry.execute() != SQLITE_OK) throw database_error("Can't save object");

    obj.id = sqlite3_int64(db->last_insert_rowid());

    for (auto field : obj.get_fields<base_field>())
    {
      if (field->save_strategy == base_field::SaveStrategy::AFTER_MASTER)
      {
        field->save();
      }
    }
  }

  static void remove(const std::shared_ptr<T> & obj, bool cascade = false)
  {
    if (obj.get())
    {
      remove(*obj.get(), cascade);
    }
  }

  static void remove(const std::string & _where)
  {
    std::string q = "DELETE FROM " + T::table_name() + " WHERE " + _where;

    sqlite3pp::command cmd(*db, q.c_str());

    cmd.execute();
  }

  static void remove(const T & obj, bool cascade = false)
  {
    if (obj.id.is_null) return;

    if (cascade)
    {
      for (auto p : obj.collection_fields)
      {
        p->set_null();
      }
    }

    std::string q = "DELETE FROM ";
    q.append(T::table_name()).append(" WHERE id = :id");

    sqlite3pp::command cmd(*db, q.c_str());

    obj.id.bind(cmd);

    cmd.execute();
  }

  static void refresh(std::shared_ptr<T> & obj)
  {
    if (!obj->_tracking)
    {
      obj = T::by_id(obj->id.value);
    }
    else
    {
      if (!obj->_update_needed) return;

      obj = T::by_id(obj->id.value);

      obj->track_changes(true);
    }
  }

  static bool exec(const std::string & _command)
  {
    return SQLITE_OK == db->execute(_command.c_str());
  }

  static void exec_throw_on_error(const std::string & _command)
  {
    if (SQLITE_OK != db->execute(_command.c_str()))
    {
      throw database_error(*db);
    }
  }
};

template <typename ModelType> database * dao<ModelType>::db;

}  // namespace orm
