#pragma once

#include "../meta/meta.h"
#include "sqlite/sqlite3pp.h"

namespace orm
{
using namespace sqlite3pp;

enum Constraints : unsigned char
{
  NONE         = 0,
  NOT_NULL     = 1,
  PRIMARY_KEY  = 0x81,
  FOREIGN_KEY  = 0x82,
  UNIQUE       = 0x83,
};

class sql_desc
{
public:
  using MetaContainer = sql_desc;

  std::string sql_name;
  std::string sql_type;
  std::string sql_select_stmt;

  Constraints sql_constraints = orm::NONE;

  sql_desc(const std::string& _sql_name,
           const std::string& _sql_type,
           const std::string& _sql_select_stmt,
           Constraints _sql_constraints)
    : sql_name(_sql_name)
    , sql_type(_sql_type)
    , sql_select_stmt(_sql_select_stmt)
    , sql_constraints(_sql_constraints)
  {
  }
};

class base_field : public meta::with_meta<sql_desc>
{
public:
  using MetaContainer = base_field;

  enum class SaveStrategy
  {
    BEFORE_MASTER,
    AFTER_MASTER,
  } save_strategy;

  virtual void set_null() = 0;
  virtual void capture(query::rows::getstream & r) = 0;
  virtual void bind(statement & q) = 0;

  virtual void save() = 0;
  virtual void load() = 0;
};

}  // namespace orm
