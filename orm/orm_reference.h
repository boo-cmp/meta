#pragma once

#include "orm_base.h"

namespace orm
{

template <typename ModelType>
class reference_field : public base_field, public meta::member_field<std::shared_ptr<ModelType>>
{
  using T = ModelType;
  using ValueType = std::shared_ptr<T>;
public:
  sqlite3_int64 reference_id = -1;

  reference_field(ValueType& _member)
    : meta::member_field<ValueType>(_member, nullptr)
  {
    save_strategy = SaveStrategy::BEFORE_MASTER;
  }

  void save() override
  {
    if (member)
    {
      orm::dao<T>::save(member);
      reference_id = member->id.value();
    }
    else
    {
      reference_id = -1;
    }
  }

  void load() override
  {
    if (reference_id != -1)
    {
      member = orm::dao<ModelType>::query_one("ID", reference_id);
    }
    else
    {
      member.reset();
    }
  }

  void set_null() override
  {
    member.reset();
    reference_id = -1;
  }

  void capture(query::rows::getstream & r) override
  {
    r >> reference_id;
  }

  void bind(statement & q) override
  {
    if (reference_id != -1)
    {
      value_binder<sqlite3_int64> binder(q, ":" + meta().sql_name, reference_id);
    }
    else
    {
      std::optional<sqlite3_int64> tmp;
      value_binder<std::optional<sqlite3_int64>> binder(q, ":" + meta().sql_name, tmp);
    }
  }
};

}  // namespace orm

