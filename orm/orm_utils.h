#pragma once

#include "orm_base.h"

#include <chrono>

namespace orm
{
using namespace sqlite3pp;

using sql_datetime = std::chrono::system_clock::time_point;

template <typename T>
struct null_setter
{
  null_setter(T& member, const T& value)
  {
    member = value;
  }
};

template <typename T>
struct null_setter<nonstd::optional<T>>
{
  null_setter(nonstd::optional<T>& member,
              const nonstd::optional<T>&)
  {
    member = nonstd::nullopt;
  }
};

template <typename T>
struct value_binder
{
  value_binder(statement& q, const std::string& name, T& member)
  {
    q.bind(name.c_str(), member);
  }
};

template <typename T>
struct value_binder<nonstd::optional<T>>
{
  value_binder(statement& q, const std::string& name, nonstd::optional<T>& member)
  {
    if (member)
    {
      value_binder<T> binder(q, name.c_str(), member.value());
    }
    else
    {
      q.bind(name.c_str(), null_type());
    }
  }
};

template <typename T>
struct value_capturer
{
  value_capturer(query::rows::getstream & r, T& member)
  {
    r >> member;
  }
};

template <>
struct value_capturer<sql_datetime>
{
  value_capturer(query::rows::getstream& r, sql_datetime& member)
  {
    int64_t v;
    r >> v;
    member = sql_datetime::clock::from_time_t(v);
  }
};

template <typename T>
struct value_capturer<nonstd::optional<T>>
{
  value_capturer(query::rows::getstream& r, nonstd::optional<T>& member)
  {
    T v;
    value_capturer<T> capture(r, v);
    member = v;
  }
};

}  // namespace orm
