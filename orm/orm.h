#pragma once

#include <chrono>

#include "orm_base.h"
#include "orm_utils.h"
#include "orm_value.h"
#include "orm_reference.h"
#include "orm_dao.h"

#include "sqlite/sqlite3pp.h"


//---------------------------- DEFINE UTILS ----------------------------
#define IS_UNIQUE(constraints) ((constraints & 0x80) == 0x80)

#define _ORM_TABLE(table, temp)\
  static const std::string& table_name() { static std::string t(table); return t; }\
  static bool is_temp() { return temp; }

#define _ORM_QUERY_TYPE(constraints, cpp_type)\
  _orm::query_type_selector<ModelType, cpp_type, IS_UNIQUE(constraints)> 

//---------------------------- INTERFACE ----------------------------

#define ORM_TABLE(table) _ORM_TABLE(table, false)
#define ORM_TEMP_TABLE(table) _ORM_TABLE(table, true)

#define VALUE_FIELD(cpp_type, name, def_value, constraints, ...)\
  cpp_type name = orm::ValueField<cpp_type>(this, name, def_value).with_meta(this, __VA_ARGS__, constraints);\
  static _ORM_QUERY_TYPE(constraints, cpp_type)::type by_##name(const cpp_type& v)\
  {\
    return _ORM_QUERY_TYPE(constraints, cpp_type)::query_by(#name, v);\
  }

#define ORM_INTEGER(cpp_type, name, def_value, constraints)\
  VALUE_FIELD(cpp_type, name, def_value, constraints, #name, "INTEGER", #name)

#define ORM_STRING(cpp_type, name, def_value, constraints)\
  VALUE_FIELD(cpp_type, name, def_value, constraints, #name, "TEXT", #name)

#define ORM_FLOAT(cpp_type, name, def_value, constraints)\
  VALUE_FIELD(cpp_type, name, def_value, constraints, #name, "REAL", #name)

#define ORM_DATETIME(name, def_value, constraints)\
  VALUE_FIELD(orm::sql_datetime, name, def_value, constraints, #name, "DATETIME", "strftime('%s'," #name ") as " #name)

#define ORM_REFERENCE(ModelType, name)\
  std::shared_ptr<ModelType> name = orm::ReferenceField<ModelType>(this, name).\
   with_meta(this, #name "_id", "INTEGER REFERENCES " + ModelType::table_name() + "(ID)", #name "_id", orm::FOREIGN_KEY);

namespace _orm
{

template <typename ModelType, typename ValueType, bool is_unique>
struct query_type_selector;

template <typename ModelType, typename ValueType>
struct query_type_selector<ModelType, ValueType, true>
{
  using type = std::shared_ptr<ModelType>;

  static type query_by(const std::string& _name, const ValueType& _v)
  {
    return orm::dao<ModelType>::query_one(_name, _v);
  }
};

template <typename ModelType, typename ValueType>
struct query_type_selector<ModelType, ValueType, false>
{
  using type = std::vector<std::shared_ptr<ModelType>>;

  static type query_by(const std::string& _name, const ValueType& _v)
  {
    return orm::dao<ModelType>::query(_name, _v);
  }
};

}  // namespace _orm

namespace orm
{
using namespace sqlite3pp;

template <typename ValueType>
using ValueField = meta::field<value_field<ValueType>>;

template <typename ModelType>
using ReferenceField = meta::field<reference_field<ModelType>>;

template <typename ModelType>
class model : public meta::model<ModelType, base_field>
{
public:
  mutable std::optional<int64_t> id =
      orm::ValueField<std::optional<int64_t>>(static_cast<ModelType*>(this), id, std::nullopt).
      with_meta(static_cast<ModelType*>(this), "id", "INTEGER", "id", orm::PRIMARY_KEY);\

  static typename _ORM_QUERY_TYPE(orm::PRIMARY_KEY, int64_t)::type by_id(const int64_t& v)
  {
    return _ORM_QUERY_TYPE(orm::PRIMARY_KEY, int64_t)::query_by("id", v);
  }
};

}  // namespace orm

