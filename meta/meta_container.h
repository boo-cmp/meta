#pragma once

#include <memory>
#include <vector>

#include "meta_field_metadata.h"

namespace meta
{

//---------------------------- static meta data ----------------------------

template <typename ModelType, typename FieldMetaType>
class fields_metadata_container
{
public:
  template <typename T> friend class field;

  using Container = std::vector<std::shared_ptr<FieldMetaType>>;

  static const Container& get_fields_metadata()
  {
    return fields_metadata;
  }

protected:
  virtual ~fields_metadata_container() {}

  static Container fields_metadata;
};

// static
template <typename ModelType, typename FieldMetaType>
typename fields_metadata_container<ModelType, FieldMetaType>::Container
    fields_metadata_container<ModelType, FieldMetaType>::fields_metadata;

//---------------------------- non-static fields ----------------------------

template <typename FieldType>
class fields_container
{
public:
  template <typename T> friend class field;

  using Container = std::vector<std::shared_ptr<FieldType>>;

  const Container& get_fields() const
  {
    return fields;
  }

protected:
  virtual ~fields_container() {}

private:
  Container fields;
};

//--------------------------------------------------------------------------

template <typename _ModelType, typename... Fields>
class model : public virtual fields_container<Fields>...
{
  template <typename FieldType> friend class fields_container;
  template <typename FieldType> friend class field;

public:
  using ModelType = _ModelType;

  model()
  {
    switch (init_status)
    {
      case MetaStatus::UNINITIALIZED: init_status = MetaStatus::INITIALIZING; break;
      case MetaStatus::INITIALIZING : init_status = MetaStatus::INITIALIZED; break;
      case MetaStatus::INITIALIZED: break;
    }
  }

  virtual ~model() {}

  template <typename FieldMetaType>
  static const typename fields_metadata_container<ModelType, FieldMetaType>::Container& get_fields_metadata()
  {
    return fields_metadata_container<ModelType, FieldMetaType>::get_fields_metadata();
  }

  template <typename FieldType>
  const typename fields_container<FieldType>::Container& get_fields() const
  {
    return static_cast<const fields_container<FieldType>*>(this)->get_fields();
  }

private:
  static enum class MetaStatus
  {
    UNINITIALIZED,
    INITIALIZING,
    INITIALIZED
  } init_status;

  bool is_inititalized() const
  {
    return init_status == MetaStatus::INITIALIZED;
  }
};

#define model_meta_status model<_ModelType, Fields...>
// static
template <typename _ModelType, typename... Fields>
typename model_meta_status::MetaStatus model_meta_status::init_status = model_meta_status::MetaStatus::UNINITIALIZED;
#undef model_meta_status

}  // namespace meta
