#pragma once

#include "meta_container.h"

#include <string>

namespace meta
{

template <typename MetaType>
class with_meta
{
  template <typename T> friend class field;
public:
  using MetaData = MetaType;

  const MetaType& meta() const
  {
    if (meta_data)
      return *meta_data;
    throw std::logic_error("Meta not initialized.");
  }

private:
  std::shared_ptr<MetaType> meta_data;
};

template <typename ValueType>
class member_field
{
public:
  member_field(ValueType& _member, const ValueType& _def_value)
    : member(_member)
    , default_value(_def_value)
  {
  }

  operator const ValueType& () const
  {
    return default_value;
  }

protected:
  ValueType& member;
  ValueType default_value;
};

template <typename ContainerType>
class collection_field : public named_field
{
public:
  collection_field(ContainerType& _collection, const std::string& _name)
    : named_field(_name)
    , collection(_collection)
  {
  }

  operator const ContainerType& () const
  {
    static ContainerType c;
    return c;
  }

protected:
  ContainerType& collection;
};

}  // namespace meta
