#pragma once

#include "meta_container.h"
#include "meta_common.h"
#include "meta_optional.h"

namespace std
{
  template <typename T>
  using optional = nonstd::optional<T>;
  using nonstd::nullopt;
  using nonstd::bad_optional_access;
  using nonstd::make_optional;
}  // namespace std

namespace meta
{

}  // namespace meta
