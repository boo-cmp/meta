#pragma once

namespace meta
{
template <typename ModelType, typename FieldMetaType> class fields_metadata_container;
template <typename FieldType> class fields_container;

template <typename FieldType>
class field : public FieldType
{
public:
  template <typename ModelType, typename... Field>
  field(fields_container<ModelType>* model, Field&&... field_info)
    : FieldType(std::forward<Field>(field_info)...)
  {
    model->fields.push_back(std::make_shared<FieldType>(*this));
  }

  template <typename ModelType, typename... Meta>
  field<FieldType>& with_meta(ModelType* model, Meta&&... meta_info)
  {
    auto meta = std::make_shared<FieldType::MetaData>(std::forward<Meta>(meta_info)...);

    if (!model->is_inititalized()) {
      fields_metadata_container<ModelType, typename FieldType::MetaData::MetaContainer>::fields_metadata
        .push_back(meta);
    }

    model->fields.back()->meta_data = meta;

    return *this;
  }
};

}
