#include <Windows.h>

#include <string>
#include <iostream>

#include "orm/orm.h"

class Ref: public orm::model<Ref>
{
public:
  ORM_TABLE("ref");
};

class Test : public orm::model<Test>
{
public:
  ORM_TABLE("test");

  ORM_STRING(std::string, name, "ololo", orm::NOT_NULL);
  ORM_DATETIME(t, orm::sql_datetime::clock::now(), orm::UNIQUE);

  ORM_STRING(std::optional<std::string>, value, std::nullopt, orm::FOREIGN_KEY);
  ORM_REFERENCE(Ref, ref);
};

int main()
{
  orm::database db;
  db.connect("C:/temp/database.db");

  try
  {
    orm::dao<Ref> ref_dao(db, true);
    orm::dao<Test> test_dao(db, true);
  } catch (const std::exception& e)
  {
    std::cout << e.what() << std::endl;
  }

  Test a;
  a.name = "abc123";
  a.ref = std::make_shared<Ref>();
  a.ref->id = 10;
  orm::dao<Test>::save(a);

  auto b = Test::by_id(a.id.value());

  auto tt = Test::get_fields_metadata<orm::sql_desc>();

  auto by_id = Test::by_id(10);
  auto by_name = Test::by_name("123");
  auto by_t = Test::by_t(orm::sql_datetime::clock::now());
  auto by_value = Test::by_value(std::nullopt);

  return 0;
}
