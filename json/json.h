#pragma once

#include "meta.h"

#include <string>

namespace json
{

class model;

class base_field
{
public:
  using MetaContainer = base_field;

  base_field(const std::string& _name)
    : name(_name)
  {
  }

  std::string name;
};

template <typename ValueType>
class value_field : public base_field
{
public:
  value_field(const std::string& _name)
    : base_field(_name)
  {
  }
};

template <typename ContainerType>
class list_field : public base_field
{
public:
};

template <typename ContainerType>
class map_field : public base_field
{
public:
};

class model : public meta::model<base_field>
{
public:
};

}  // namespace json
